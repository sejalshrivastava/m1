import React, {Component} from "react";
class M1_AN_A6_Match extends Component{
    state ={
        team1:this.props.team1,
        team2:this.props.team2,
        team1Score:0,
        team2Score:0,
    };
    handleScore=(str)=>{
        let s1={...this.state};
        str==="team1"
        ?s1.team1Score=s1.team1Score+1
        :s1.team2Score=s1.team2Score+1;
        this.setState(s1);
    }
    handleSubmit=()=>{
        let s1={...this.state};        
        let winner=s1.team1Score>s1.team2Score
            ?s1.team1+" Won"
            :s1.team2Score>s1.team1Score
            ?s1.team2+" Won"
            :"Match Drawn";
        let arr={team1:s1.team1,team2:s1.team2,team1Score:s1.team1Score,team2Score:s1.team2Score,winner:winner};
        this.props.onSubmit(0,arr);
    }
    render(){
        let {team1,team2,team1Score,team2Score}=this.state;
        return (
            <div className="container text-center">
                <h4 className="m-1">Welcome to an exciting Match</h4>
                <div className="row m-3">
                    <div className="col-4">
                        <h5>{team1}</h5>
                        <button className="btn btn-warning"  onClick={()=>this.handleScore("team1")}>
                                Goal Scored</button>
                    </div>
                    <div className="col-4">
                        <h3>{team1Score}-{team2Score}</h3>
                    </div>
                    <div className="col-4">
                    <h5>{team2}</h5>
                        <button className="btn btn-warning" onClick={()=>this.handleScore("team2")}>
                                Goal Scored</button>                    
                    </div>
                </div>
                <button className="btn btn-warning" onClick={()=>this.handleSubmit()}>Match Over</button>
            </div>
        )
    }
}
export default M1_AN_A6_Match;