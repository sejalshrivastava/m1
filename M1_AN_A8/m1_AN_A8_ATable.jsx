import React,{Component} from "react";
class M1_AN_A8_ATable extends Component{
    state={
        Products:this.props.Products,
        forFilter:{category:"",inStock:"",price:""},
        heads:["Code","Product","Category","Price","In Stock"],
        headsCpy:["Code","Product","Category","Price","In Stock"],
        sortBy:-1,
    };
    handleChange=(e)=>{
        let {currentTarget:input}=e;
        let s1={...this.state};
        s1.forFilter[input.name]=input.value;        
        this.setState(s1);
    }
    makedropDown=(arr,name,label,value)=>
        <div className="form-group">
            <select className="form-control" 
                onChange={this.handleChange}
                name={name} 
                value={value}>
            <option value="">{label}</option>        
            {
                arr.map((opt)=>(<option key={opt} >{opt}</option>))
            }
            </select>
        </div>;
     filterPrice=(str,arr)=>{
        if(str[0]===">"){
            let n=+str.substring(1,str.length);
            let arr1=arr.filter(ar=>ar.price>n);
            return arr1;
        }
        else if(str[0]==="<"){
            let n=+str.substring(1,str.length);
            let arr1=arr.filter(ar=>ar.price<n);
            return arr1;
        }
        else
        {
            let n=str.split("-");
            let arr1=arr.filter(ar=>ar.price>=n[0]&&ar.price<=n[1]);
            return arr1;
        }
    }
    handleBill=(code)=>{
        this.props.addToBill(code);
    }
    handleSort=(digit)=>{
        let s1={...this.state};
        this.props.onSort(digit);
        s1.sortBy=digit;   
        s1.heads=[...s1.headsCpy ];       
        if(s1.sortBy>=0)s1.heads[s1.sortBy]=s1.heads[s1.sortBy]+"(X)"; 
        this.setState(s1);
    }
    render(){
        let {Products,heads}=this.state;        
        let {category,inStock,price}=this.state.forFilter;
        let categories=["Beverages","Chocolates","Biscuits"];
        let inStocks=["Yes","No"];
        let prices=["<10","10-20",">20"];
        let Products1=category?Products.filter(pr=>pr.category===category):Products;    
        let Products2=inStock?Products1.filter(pr=>pr.instock===inStock):Products1; 
        let Products3=price?this.filterPrice(price,Products2):Products2;
        
        
        return <div className="container">
            <h3 className="text-center">Product List</h3>
            <div className="row text-center">
                <div className="col-3">
                <b>Filter Products By :</b>
                </div>
                <div className="col-3">
                    {this.makedropDown(categories,"category","Select Category",category)}
                </div>
                <div className="col-3">
                    {this.makedropDown(inStocks,"inStock","Select In Stock",inStock)}
                </div>
                <div className="col-3">
                {this.makedropDown(prices,"price","Select Price Range",price)}
                </div>
            </div>
                    <div className="row border bg-dark text-white mt-1" >
                    <div className="col-2" onClick={()=>this.handleSort(0)}>{heads[0]}</div>
                    <div className="col-2" onClick={()=>this.handleSort(1)}>{heads[1]}</div>
                    <div className="col-2" onClick={()=>this.handleSort(2)}>{heads[2]}</div>
                    <div className="col-2" onClick={()=>this.handleSort(3)}>{heads[3]}</div>
                    <div className="col-2" onClick={()=>this.handleSort(4)}>{heads[4]}</div>
                    <div className="col-2"></div>
                    </div>
            
            {
                Products3.map((pr,index)=><div className="row border" key={pr.code}>
                    <div className="col-2">{pr.code}</div>
                    <div className="col-2">{pr.prod}</div>
                    <div className="col-2">{pr.category}</div>
                    <div className="col-2">{pr.price}</div>
                    <div className="col-2">{pr.instock}</div>
                    <div className="col-2">
                        <button className="btn btn-secondary btn-sm m-1"
                            onClick={()=>this.handleBill(pr.code)}>Add to Bill</button>
                    </div>
                </div>)
            }
        </div>
    }
}
export default M1_AN_A8_ATable;