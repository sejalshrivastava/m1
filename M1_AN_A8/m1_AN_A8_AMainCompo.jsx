import React,{Component} from "react";
import M1_AN_A8_ABill from "./m1_AN_A8_ABill";
import M1_AN_A8_ATable from "./m1_AN_A8_ATable";
class M1_AN_A8_AMainCompo extends Component{
    state={
       Products: 
        [        
        {code:"PEP221",prod:"Pepsi",price:12,instock:"Yes",category:"Beverages"},
		{code:"COK113",prod:"Coca Cola",price:18,instock:"Yes",category:"Beverages"},
		{code:"MIR646",prod:"Mirinda",price:15,instock:"No",category:"Beverages"},
		{code:"SLI874",prod:"Slice",price:22,instock:"Yes",category:"Beverages"},
		{code:"MIN654",prod:"Minute Maid",price:25,instock:"Yes",category:"Beverages"},
		{code:"APP652",prod:"Appy",price:10,instock:"No",category:"Beverages"},
		{code:"FRO085",prod:"Frooti",price:30,instock:"Yes",category:"Beverages"},
		{code:"REA546",prod:"Real",price:24,instock:"No",category:"Beverages"},
		{code:"DM5461",prod:"Dairy Milk",price:40,instock:"Yes",category:"Chocolates"},
		{code:"KK6546",prod:"Kitkat",price:15,instock:"Yes",category:"Chocolates"},
		{code:"PER5436",prod:"Perk",price:8,instock:"No",category:"Chocolates"},
		{code:"FST241",prod:"5 Star",price:25,instock:"Yes",category:"Chocolates"},
		{code:"NUT553",prod:"Nutties",price:18,instock:"Yes",category:"Chocolates"},
		{code:"GEM006",prod:"Gems",price:8,instock:"No",category:"Chocolates"},
		{code:"GD2991",prod:"Good Day",price:25,instock:"Yes",category:"Biscuits"},
		{code:"PAG542",prod:"Parle G",price:5,instock:"Yes",category:"Biscuits"},
		{code:"MON119",prod:"Monaco",price:7,instock:"No",category:"Biscuits"},
		{code:"BOU291",prod:"Bourbon",price:22,instock:"Yes",category:"Biscuits"},
		{code:"MAR951",prod:"MarieGold",price:15,instock:"Yes",category:"Biscuits"},
		{code:"ORE188",prod:"Oreo",price:30,instock:"No",category:"Biscuits"},		
        ],
        view:0,
        bills:[],
    };
    handleView=(vw)=>{
        let s1={...this.state};
        s1.view=vw;
        this.setState(s1);
    }
    Sort=(col)=>{
        let s1={...this.state};
        switch(col){
            case 0:s1.Products.sort((p1,p2)=>p1.code.localeCompare(p2.code));break;
            case 1:s1.Products.sort((p1,p2)=>p1.prod.localeCompare(p2.prod));break;
            case 2:s1.Products.sort((p1,p2)=>p1.category.localeCompare(p2.category));break;
            case 3:s1.Products.sort((p1,p2)=>p1.price-p2.price);break;
            case 4:s1.Products.sort((p1,p2)=>p1.instock.localeCompare(p2.instock));break;            
        }        
        this.setState(s1);
    }
    handleBill=(code)=>{
        let s1={...this.state};
        let found=s1.bills.find(bl=>bl.code===code);
        if(found){found.quantity=found.quantity+1}
        else{ 
            let index=s1.Products.findIndex(pr=>pr.code===code)
            s1.bills.push(
                    {code:s1.Products[index].code,prod:s1.Products[index].prod,quantity:1,price:s1.Products[index].price})}    
        this.setState(s1);
    }
    removeToBill=(code)=>{
        let s1={...this.state};
        let found=s1.bills.findIndex(bl=>bl.code===code);
        s1.bills.splice(found,1);
        this.setState(s1);
    }
    reduceToBill=(code)=>{
        let s1={...this.state};
        let found=s1.bills.findIndex(bl=>bl.code===code);
        
        if(s1.bills[found].quantity===1)
        {
            s1.bills.splice(found,1);
        }
        else{
            s1.bills[found].quantity=s1.bills[found].quantity-1;
        }
        this.setState(s1);
    }
   emptyBill=()=>{
    let s1={...this.state};
    s1.bills=[];
    s1.view=0;
    this.setState(s1);
   }
    render(){
        let {Products,view,bills}=this.state;
                        
        return <div className="container">
            <nav className="navbar navbar-expand-sm navbar-dark bg-dark">            
              <a className="navbar-brand " > Billing System</a>              
              <div className="collapse navbar-collapse">
                <ul className="navbar-nav">
                  <li className="nav-item">
                    <a className="nav-link " onClick={()=>this.handleView(1)} >New Bill</a>
                  </li>                                   
                </ul>
              </div>            
          </nav>
        {
            view===1
            ?<React.Fragment>
                <M1_AN_A8_ABill bills={bills}
                    exceedProd={this.handleBill}
                    reduceProd={this.reduceToBill}
                    handleRem={this.removeToBill}
                    clearBills={this.emptyBill}
                     />
                <M1_AN_A8_ATable 
                    Products={Products} 
                    onSort={this.Sort}
                    addToBill={this.handleBill} />
            </React.Fragment>
            :""
        }    

        </div>
    }
}
export default M1_AN_A8_AMainCompo;