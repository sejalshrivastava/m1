import React, {Component} from "react";
import queryString from "query-string"; 
import M1_AN_A8_CLeftPanel from "./m1_AN_A8_CLeftPanel";   
import M1_AN_A8_CBottomBtn from "./m1_AN_A8CBottomBtn";
class M1_AN_A8_CEmps extends Component{
  
    filterParams=(arr,queryParams)=>{
        let {department,designation}=queryParams;        
        arr=department?this.filterParam(arr,"department",department):arr;
        arr=designation?this.filterParam(arr,"designation",designation):arr;                
        return arr;
    }    
    filterParam=(arr,name,values)=>{        
        let valuesArr=values.split(",");
        let arr1=arr.filter((a1)=>valuesArr.find((val)=>val===a1[name]));
        return arr1;
    }
    addToQueryString=(str,paramName,paramValue)=>
        paramValue
            ?str
            ?`${str}&${paramName}=${paramValue}`
            :`${paramName}=${paramValue}`
        :str;
    handleOptionChange=(options)=>{        
        let {location}=this.props.match.params;
            {location?this.callURL(`/emps/${location}`,options):this.callURL("/emps",options);}
        }      
    callURL=(url,options)=>{
            let searchString=this.makeSearchString(options);            
            this.props.history.push(
                {
                 search:searchString,   
                });
        };    
    makeSearchString=(options)=>{
        let {department,designation,page}=options;        
        let searchStr="";
        searchStr=this.addToQueryString(searchStr,"department",department) ;
        searchStr=this.addToQueryString(searchStr,"designation",designation) ;
        searchStr=this.addToQueryString(searchStr,"page",page);
        
        return searchStr;
    } 
    makeAllOptions=(arr)=>{        
        let json={};        
        json.department=this.getDifferentValues(arr,"department");
        json.designation=this.getDifferentValues(arr,"designation");              
        return json;
    } 
    getDifferentValues=(arr,name)=>    
     arr.reduce (
         (acc,curr)=>
            acc.find((val)=>val===curr[name])?acc:[...acc,curr[name]],[]
     );
    render(){
        let {Employees}=this.props;
        
        const {location}=this.props.match.params;
        let arr=this.makeAllOptions(Employees);        
        let queryParams=queryString.parse(this.props.location.search);
        queryParams.page=queryParams.page?queryParams.page:1;
        
        let {page}=queryParams;
        let Employees1=location?Employees.filter(emp=>emp.location===location):Employees;
        let Employees2= this.filterParams(Employees1,queryParams);
        let size=2;
        let startIndex=page>1?(page-1):(page-1)*size;
        let endIndex=Employees1.length>startIndex+size-1
                ?startIndex + size-1
                :Employees1.length-1;
        let Employees3=Employees2.length>2 
            ?Employees2.filter((lt,index)=>{return index >=startIndex && index<=endIndex})
            :Employees2;          
        return <div className="row">
                <div className="col-2">
                <M1_AN_A8_CLeftPanel 
                    allOptions={arr} options={queryParams} 
                    onChange={this.handleOptionChange} />
                </div>
                <div className="col-10">
                    <h3 className="text-center">Welcome to Employee Portal</h3>
                    <b>Your have choosen</b>
                    <br/>
                    Location : {location?location:"All"}
                    <br/>
                    Department : {queryParams.department?queryParams.department:"All"}
                    <br/>
                    Designation :{queryParams.designation?queryParams.designation:"All"}
                    <br/><br/>
                    The number of employees matching the options : {Employees2.length}
                    <div className="row">
                        { 
                            Employees3.map((emp,index)=><div className="col-6 border" key={index}>
                                <b>{emp.name}</b>
                                <br/>
                                {emp.email}
                                <br/>
                                Mobile : {emp.mobile}
                                <br/>
                                Location : {emp.location}
                                <br/>
                                Department : {emp.department}
                                <br/>
                                Designation : {emp.designation}
                                <br/>
                                Salary : {emp.salary}
                            </div>)
                        }
                    </div>
                    <M1_AN_A8_CBottomBtn options={queryParams} 
                       len={Employees2.length} onChange={this.handleOptionChange} />
                    
                </div>
            </div>
                
    }
}
export default M1_AN_A8_CEmps;