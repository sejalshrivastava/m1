import React, { Component } from "react";
class M1_AN_A8_CBottomBtn extends Component{
   
    handlePage=(btn)=>{
        let {options}=this.props;      
        if (btn==="Next")
        {
            options.page=options.page?+options.page+1:2;
        }
        else
        {
            options.page=+options.page-1;
        }
        this.props.onChange(options);
    }
    
    render(){
        let {options,len}=this.props;      
        
        let {page}=options;        
        
        return <div className="row m-1">
        <div className="col-2">
            {page>1?
        <button className="btn btn-primary" onClick={()=>this.handlePage("back")}>Previous</button>
            :""}</div>
        <div className="col-8"></div>
        <div className="col-2">
        {page<len-1||!page?
        <button className="btn btn-primary" onClick={()=>this.handlePage("Next")}>Next</button>
            :""}
            </div>    
            </div>
       
    }
}
export default M1_AN_A8_CBottomBtn;