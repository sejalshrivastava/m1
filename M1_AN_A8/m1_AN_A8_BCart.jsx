import React,{Component} from "react";
class M1_AN_A8_BCart extends Component{
    state={
       cart:this.props.cart,
    };
    
    addToCart=(id)=>{
        let s1={...this.state}  ;
        let Product=s1.cart.find(cr=>cr.id===id)      ;
        this.props.exceed(Product);
    }    
    removeToCart=(id)=>{
        this.props.removeFromCart(id)
    }
    render(){
        let {cart}=this.props;                              
        
        return <React.Fragment>
            <h1 className="text-center">Cart</h1>
            
            <div className="row">
                {
                    cart.map(cr=>{ 
                        let {image,id,desc,name,size,crust,quantity}=cr;
                        return cr.type==="Pizza"?<div className="row m-1">
                            <div className="col-6"><img src={image} width="98%"></img></div>
                            <div className="col-6">
                                <h5>{name}</h5>
                                {desc}
                                <h6>{size}|{crust}</h6>
                                <button className="btn btn-danger m-1 text-white"
                                onClick={()=>this.removeToCart(id)}>-</button>
                            <button className="btn btn-secondary m-1 text-white">{quantity}</button>
                            <button className="btn btn-success m-1 text-white" 
                                onClick={()=>this.addToCart(id)}>+</button>  
                            </div>
                        </div>                        
                        :
                        <div className="row m-1">
                            <div className="col-6"><img src={image} width="98%"></img></div>
                            <div className="col-6">
                                <h5>{name}</h5>
                                {desc}                      
                                <br/>          
                                <button className="btn btn-danger m-1 text-white"
                                onClick={()=>this.removeToCart(id)}>-</button>
                            <button className="btn btn-secondary m-1 text-white">{quantity}</button>
                            <button className="btn btn-success m-1 text-white" 
                                onClick={()=>this.addToCart(id)}>+</button>  
                            </div>
                        </div>      
                    })
                }
                    
                    </div>
                </React.Fragment>         
            
    }
}
export default M1_AN_A8_BCart;