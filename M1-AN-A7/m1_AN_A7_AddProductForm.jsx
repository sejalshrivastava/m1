import React,{Component} from "react";
class M1_AN_A7_AddProductForm extends Component{
    state={
        product:this.props.product,
        editIndex:this.props.editIndex,
        errors:{},
    };
    handleView=(view)=>{
        this.props.changeView(view);
    }
    handleChange=(e)=>{
        let {currentTarget:input}=e;
        let s1={...this.state};        
        input.type==="checkbox"
            ?s1.product[input.name]=input.checked
            :s1.product[input.name]=input.value;
        this.setState(s1);
    }
    handleSubmit=()=>{
        let {product,errors,editIndex}=this.state;
        product.quantity=editIndex===-1?0:product.quantity;
        if(this.validate(product))
        {
        this.props.onSubmit(product);
        }
        else{
            if(errors.category) alert(errors.category)
            else if(errors.brand) alert(errors.brand)
        }
    }
    validate=(product)=>{
        let s1={...this.state};
        let {codes}=this.props;        
        s1.errors.code=product.code?codes.find(cd=>cd===product.code)?s1.editIndex>=0?"":"Code Already Exist":"":"Product Code is Required";
        s1.errors.price=product.price?"":"Price is Required";
        s1.errors.category=product.category?"":"Category is Required";
        s1.errors.brand=product.brand?"":"Brand is Required";
        this.setState(s1);        
        return (!s1.errors.code&&!s1.errors.price&&!s1.errors.category&&!s1.errors.brand);
    }
    render(){
        let {code,price,category,brand,specialOffer,limitedStock}=this.state.product; 
        let {errors}=this.state;       
        let {editIndex,Brands}=this.props;
        
        let categories=["Food","Personal Care","Apparel"];
        let brands=category==="Food"?Brands.slice(0,6)
            :category==="Personal Care"?Brands.slice(6,11)
            :category==="Apparel"?Brands.slice(11,15):[];
        
        return <div className="container">
            <div className="form-group">
                <label className="form-label">Product Code</label>
                <input className="form-control"
                    type="text"
                    placeholder="Enter Product Code"
                    value={code}
                    name="code"
                    disabled={editIndex>=0}
                    onChange={this.handleChange}
                      />
                {errors.code?<span className="text-danger">{errors.code}</span>:""}
            </div>
            <div className="form-group">
                <label className="form-label">Price </label>
                <input className="form-control"
                    type="text"
                    placeholder="Enter Price"
                    value={price}
                    name="price"                    
                    onChange={this.handleChange}
                      />
                    {errors.price?<span className="text-danger">{errors.price}</span>:""}  
            </div>            
            <label className="form-check-label"><b>Category</b></label><br/>
            {
                categories.map((opt)=>(
                    <div className="form-check-inline" key={opt}>
                        <input 
                        type="radio"
                        value={opt}
                        name="category"
                        checked={category===opt}
                        onChange={this.handleChange} />
                        <label className="form-check-inline">{opt}</label>
                    </div>
                ))
            }
            
            <div className="form-group m-2">
            <select
                className="form-control"
                name="brand"
                value={brand}
                onChange={this.handleChange} >
                    <option value="">Select Brand</option>
                {
                   brands.map(opt=><option>{opt}</option>)
                }    
           </select>
            </div>
            <label><b>Choose other info about the product</b></label>
            <br/>
            <input className="form-check-inline"
                type="checkbox"
                name="specialOffer"
                checked={specialOffer}
                value={specialOffer}
                onChange={this.handleChange} />
            <label className="label-check-inline">Special Offer</label>
            <br/>
            <input className="form-check-inline"
                type="checkbox"
                name="limitedStock"
                checked={limitedStock}
                value={limitedStock}
                onChange={this.handleChange} />
            <label className="label-check-inline">Limited Stock</label>  
            <br/>

            <button className="btn btn-primary mt-1 btn-sm"
                onClick={()=>this.handleSubmit()} >{editIndex>=0?"Edit Product":"Add Product"}</button>
            <br></br>
            <button className="btn btn-primary mt-4" onClick={()=>this.handleView(0)} >Go Back To Homepage</button>
        </div>
    }
}
export default M1_AN_A7_AddProductForm;