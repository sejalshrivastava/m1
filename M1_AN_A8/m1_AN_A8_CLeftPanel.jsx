import React, {Component} from "react";

class M1_AN_A8_CLeftPanel extends Component{
    handleChange=(e)=>{
        let {currentTarget:input}=e;
        let options ={...this.props.options};
        input.type==="checkbox"
        ?options[input.name]=this.updateCBs(options[input.name],input.checked,input.value)
        :options[input.name]=input.value;
        options.page=1;
        this.props.onChange(options);
     };
     updateCBs=(inpValue,checked,value)=>{
         let inpArr=inpValue? inpValue.split(","):[];
         if (checked) inpArr.push(value);
         else{
             let index=inpArr.findIndex((ele)=>ele===value);
             if (index>=0) inpArr.splice(index,1);
         }
         return inpArr.join(",");
     };
     makeCheckBoxes=(arr,name,label,values)=>(        
       <React.Fragment>             
             <label className="form-check-label"><b>{label}</b></label>
             {
                 arr.map((opt)=>(
                     <div className="form-check" key={opt}>
                         <input 
                         type="checkbox"
                         value={opt}
                         name={name}
                         checked={values.findIndex((val)=>val===opt)>=0}
                         onChange={this.handleChange} />
                         <label className="form-check-inline">{opt}</label><br/>
                     </div>
                 ))
             }
         </React.Fragment>
     )
     makeRadios=(arr,name,label,value)=>(
        <React.Fragment>
            <label className="form-check-label "><b>{label}</b></label><br/>
            {
                arr.map((opt)=>(
                    <div className="form-check-inline" key={opt}>
                        <input 
                        type="radio"
                        value={opt}
                        name={name}
                        checked={value===opt}
                        onChange={this.handleChange} />
                        <label className="form-check-inline">{opt}</label>
                    </div>
                ))
            }
        </React.Fragment>
    )
   
    render(){
        let {allOptions}=this.props;
        let {department,designation}=this.props.options;  
        department=department?department:"";
        designation=designation?designation:"";
        return <div className="container">
            <div className="row ">
                <div className="col-12 ">
                {this.makeRadios(allOptions.designation,"designation","Designation",designation)}
                </div>
                <div className="col-12 ">
                {this.makeCheckBoxes(allOptions.department,"department","Department",department.split(","))}
                </div>
                   
               </div> 
        </div>
        
    }
}
export default M1_AN_A8_CLeftPanel;