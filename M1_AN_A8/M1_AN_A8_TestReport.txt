Section A:
------------------
Connsist of three component- m1_AN_A8_AMainCompo , m1_AN_A8_ABill , m1_AN_A8_ATable
*It has never with component new bill
*On clicking new bill datails of bill will open , it shows current bill and product list.
*Products in product table can sort in ascending order by clicking on column head.
*On Selecting category , in stock and price range from drop down will filter the table according to the provided values of category , in stock and price range.
*On clicking add to bill of the row in table will add the product in current table and if exist it will increase the quantity by one.
*In current bill table the total number of items , quantity and amount are shown .
*In currenttable every row consists three buttons  '+,-,x' .
* On clicking  '+' increase the quantity of that product .
*On clicking  '-' decrease the quantity of that product .
*On clicking  'x' it removes product from current bill .
*It has close bill button , On clicking it will close current bill and return to home screen .
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Section B:
-----------------
Consist of six Components:- m1_AN_A8_BMainCompo , m1_AN_A8_BCart , m1_AN_A8_BLeftPanelNonVeg , m1_AN_A8_BLeftPanelOther , m1_AN_A8_BLeftPanelSide , m1_AN_A8_BLeftPanelVeg , 
*Home screen shows a nav bar.
*On clicking components in nav bar ,shows the table of products filtered by selected category in nav bar and in right side , it shows a cart in all pages.
*In veg-pizza and non-veg pizza to add to cart have to select size and crust .
*On add to cart it will show in cart.
*Cart item will not show add to cart button it will show ' + , - , number of quantity'  button with selected size and crust similarly in cart.
*In side dishes and others to add to cart it will not show dropdown to select size and crust they dont needed
*On add to cart it will show in cart.
*Cart Values have image ,details and three buttons '+,- ,numbet of quantity of that item' button.
* number of item button is not clickable.
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Section C:
----------------- 
Consist of four Components:-m1_AN_A8_CMainCompo ,m1_AN_A8_CNavbar ,m1_AN_A8_CEmps ,m1_AN_A8_CLeftPanel.
*Initially it show a navbar unfilterted employees data.
*Navbar components are routed with two different paths .
*In left, radios for selecting designation and checkboxes for departments are provided
*If it is first page it will not show previous button.
*If it is last page it will not show next button.
*On changing any condition it will reached to you on first page.
*On changing page will show in url.
*On selecting disselecting radios and checkbox will change queryParameter. 



******************
All Sections working fine
No Error found