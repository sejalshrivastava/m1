import React,{Component} from "react";
class M1_AN_A8_BLeftPanelVeg extends Component{
    state={
        Product:this.props.Product,
    };
    handleChange=(e)=>{
        let {currentTarget:input}=e;
        let s1={...this.state};
        s1.Product[input.name]=input.value;
        this.setState(s1);
    }
    makedropDown=(arr,name,label,value)=>
        <div className="form-group">
            <select className="form-control" 
                onChange={this.handleChange}
                name={name} 
                value={value}>
            <option value="">{label}</option>        
            {
                arr.map((opt)=>(<option key={opt} >{opt}</option>))
            }
            </select>
        </div>;
    addToCart=()=>{
        let{Product}=this.state;
        if(Product.size===""){ alert("Choose the Size before adding to Cart");}
        else if(Product.crust===""){ alert("Choose the Crust before adding to Cart");}
        else{this.props.exceed(Product);}
    }    
    removeToCart=(id)=>{
        this.props.removeFromCart(id)
    }
    render(){
        let {sizes,crusts,cart}=this.props;
        let {Product}=this.state;        
        let {id,image,name,desc,size,crust}=Product;                         
        let inCart=cart.find(cr=>cr.id===id);
        return <React.Fragment>
            {inCart?<div className="col-6 text-center border">                        
                        <img src={image} width="94%"></img>
                        <h4>{name}</h4>
                        {desc}
                        <div className="row m-2">
                            <div className="col-6">
                                {this.makedropDown(sizes,"size","Select Size",inCart.size)}
                            </div>
                            <div className="col-6">
                                {this.makedropDown(crusts,"crust","Select Crust",inCart.crust)}
                            </div>
                        </div>
                        <button className="btn btn-danger m-1 text-white"
                            onClick={()=>this.removeToCart(id)}>-</button>
                        <button className="btn btn-secondary m-1 text-white">{inCart.quantity}</button>
                        <button className="btn btn-success m-1 text-white" 
                            onClick={()=>this.addToCart()}>+</button>
                    
                    </div>
                    :<div className="col-6 text-center border">                        
                        <img src={image} width="94%"></img>
                        <h4>{name}</h4>
                        {desc}
                        <div className="row m-2">
                            <div className="col-6">
                                {this.makedropDown(sizes,"size","Select Size",size)}
                            </div>
                            <div className="col-6">
                                {this.makedropDown(crusts,"crust","Select Crust",crust)}
                            </div>
                        </div>
                        <button className="btn btn-primary m-1" 
                            onClick={()=>this.addToCart()}>Add to Cart</button>
                    
                    </div>
                    }
                </React.Fragment>         
            
    }
}
export default M1_AN_A8_BLeftPanelVeg;