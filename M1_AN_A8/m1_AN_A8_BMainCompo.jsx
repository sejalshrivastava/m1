import React,{Component} from "react";
import M1_AN_A8_BLeftPanelVeg from "./m1_AN_A8_BLeftPanelVeg";
import M1_AN_A8_BLeftPanelNonVeg from "./m1_AN_A8_BLeftPanelNonVeg";
import M1_AN_A8_BLeftPanelSide from "./m1_AN_A8_BLeftPanelSide";
import M1_AN_A8_BLeftPanelOther from "./m1_AN_A8_BLeftPanelOther";
import M1_AN_A8_BCart from "./m1_AN_A8_BCart";
class M1_AN_A8_BMainCompo extends Component{
    state={
        sizes : ["Regular","Medium","Large"],
        crusts : ["New Hand Tossed","Wheat Thin Crust","Cheese Burst","Fresh Pan Pizza","Classic Hand Tossed"],

Products:[
{id:"MIR101",image:"https://i.ibb.co/SR1Jzpv/mirinda.png",type:"Beverage",name:"Mirinda",desc:"Mirinda",veg:"Yes"},
{id:"PEP001",image:"https://i.ibb.co/3vkKqsF/pepsiblack.png",type:"Beverage",name:"Pepsi Black Can",desc:"Pepsi Black Can",veg:"Yes"},
{id:"LIT281",image:"https://i.ibb.co/27PvTng/lit.png",type:"Beverage",name:"Lipton Iced Tea",desc:"Lipton Iced Tea",veg:"Yes"},
{id:"PEP022",image:"https://i.ibb.co/1M9xDZB/pepsi-new20190312.png",type:"Beverage",name:"Pepsi New",desc:"Pepsi New",veg:"Yes"},
{id:"BPCNV1",image:"https://i.ibb.co/R0VSJjq/Burger-Pizza-Non-Veg-nvg.png",type:"Burger Pizza",name:"Classic Non Veg",desc:"Oven-baked buns with cheese, peri-peri chicken, tomato & capsicum in creamy mayo",veg:"No"},
{id:"BPCV03",image:"https://i.ibb.co/Xtx43fT/Burger-Pizza-Veg-423-X420-Pixel1.png",type:"Burger Pizza",name:"Classic Veg",desc:"Oven-baked buns with cheese, tomato & capsicum in creamy mayo",veg:"Yes"},
{id:"BPPV04",image:"https://i.ibb.co/Xtx43fT/Burger-Pizza-Veg-423-X420-Pixel1.png",type:"Burger Pizza",name:"Premium Veg","desc":"Oven-baked buns with cheese, paneer,tomato, capsicum & red paprika in creamy mayo",veg:"Yes"},
{id:"DES028",image:"https://i.ibb.co/nm96NZW/ChocoLava.png",type:"Dessert",name:"Choco Lava Cake",desc:"Chocolate lovers delight! Indulgent,gooey molten lava inside chocolate cake",veg:"Yes"},
{id:"DES044",image:"https://i.ibb.co/gvpDKPv/Butterscotch.png",type:"Dessert",name:"Butterscotch Mousse Cake",desc:"Sweet temptation! Butterscotch flavored mousse",veg:"Yes"},

{id:"DIP033",image:"https://i.ibb.co/0mbBzsw/new-cheesy.png",type:"Side Dish",name:"Cheesy Dip",desc:"An all-time favorite with your Garlic Breadsticks & Stuffed Garlic Bread for a Cheesy indulgence",veg:"Yes"},
{id:"DIP072",image:"https://i.ibb.co/fY52zBw/new-jalapeno.png",type:"Side Dish",name:"Cheesy Jalapeno Dip",desc:"A spicy, tangy flavored cheese dip is a an absolute delight with your favourite Garlic Breadsticks",veg:"Yes"},
{id:"GAR952",image:"https://i.ibb.co/BNVmfY9/Garlic-bread.png",type:"Side Dish",name:"Garlic Breadsticks",desc:"Baked to perfection. Your perfect pizza partner! Tastes best with dip",veg:"Yes"},
{id:"PARCH1",image:"https://i.ibb.co/prBs3NJ/Parcel-Nonveg.png",type:"Side Dish",name:"Chicken Parcel",desc:"Snacky bites! Pizza rolls with chicken sausage & creamy harissa sauce",veg:"No"},
{id:"PARVG7",image:"https://i.ibb.co/JHhrM7d/Parcel-Veg.png",type:"Side Dish",name:"Veg Parcel",desc:"Snacky bites! Pizza rolls with paneer & creamy harissa sauce",veg:"Yes"},
{id:"PATNV7",image:"https://i.ibb.co/0m89Jw9/White-Pasta-Nvg.png",type:"Side Dish",name:"White Pasta Italiano Non-Veg",desc:"Creamy white pasta with pepper barbecue chicken",veg:"No"},
{id:"PATVG4",image:"https://i.ibb.co/mv8RFbk/White-Pasta-Veg.png",type:"Side Dish",name:"White Pasta Italiano Veg",desc:"Creamy white pasta with herb grilled mushrooms",veg:"Yes"},

{id:"PIZVDV",image:"https://i.ibb.co/F0H0SWG/deluxeveg.png",type:"Pizza",name:"Deluxe Veggie",desc:"Veg delight - onion, capsicum, grilled mushroom, corn & paneer",veg:"Yes"},
{id:"PIZVFH",image:"https://i.ibb.co/4mHxB5x/farmhouse.png",type:"Pizza",name:"Farmhouse",desc:"Delightful combination of onion, capsicum, tomato & grilled mushroom",veg:"Yes"},
{id:"PIZVIT",image:"https://i.ibb.co/sRH7Qzf/Indian-TandooriPaneer.png",type:"Pizza",name:"Indi Tandoori Paneer",desc:"It is hot. It is spicy. It is oh-soIndian. Tandoori paneer with capsicum, red paprika & mint mayo",veg:"Yes"},
{id:"PIZVMG",image:"https://i.ibb.co/MGcHnDZ/mexgreen.png",type:"Pizza",name:"Mexican Green Wave",desc:"Mexican herbs sprinkled on onion, capsicum, tomato &jalapeno",veg:"Yes"},
{id:"PIZVPP",image:"https://i.ibb.co/cb5vLX9/peppypaneer.png",type:"Pizza",name:"PeppyPaneer",desc:"Flavorful trio of juicy paneer, crisp capsicum with spicy red paprika",veg:"Yes"},
{id:"PIZVVE",image:"https://i.ibb.co/gTy5DTK/vegextra.png",type:"Pizza",name:"VegExtravaganza",desc:"Black olives, capsicum, onion, grilled mushroom, corn, tomato, jalapeno & extra cheese",veg:"Yes"},

{id:"PIZNCP",image:"https://i.ibb.co/b5qBJ9d/cheesepepperoni.png",type:"Pizza",name:"Chicken Pepperoni",desc:"A classic American taste! Relish the delectable flavor of Chicken Pepperoni,topped with extra cheese",veg:"No"},
{id:"PIZNCD",image:"https://i.ibb.co/GFtkbB1/ChickenDominator10.png",type:"Pizza",name:"Chicken Dominator",desc:"Loaded with double pepperbarbecue chicken, peri-peri chicken, chicken tikka & grilled chicken rashers",veg:"No"},
{id:"PIZNPB",image:"https://i.ibb.co/GxbtcLK/Pepper-Barbeque-OnionC.png",type:"Pizza",name:"Pepper Barbecue & Onion",desc:"A classic favourite with pepperbarbeque chicken & onion",veg:"No"},
{id:"PIZNIC",image:"https://i.ibb.co/6Z5wBqr/Indian-Tandoori-ChickenTikka.png",type:"Pizza",name:"Indi Chicken Tikka",desc:"The wholesome flavour of tandoorimasala with Chicken tikka, onion, red paprika & mint mayo",veg:"No"}
	],
	cart:[],
    
    view:0,
    };
    
    handleView=(vw)=>{
        let s1={...this.state};
        s1.Products=s1.Products.map(pr=>{return {...pr,size:"",crust:""}});
        s1.view=vw;
        this.setState(s1);
    }
    addToCart=(Product)=>{
        let s1={...this.state};
        let found=s1.cart.findIndex(cr=>cr.id===Product.id);
        if(found>=0)
        {
            s1.cart[found].quantity=s1.cart[found].quantity+1;
        }
        else
        {
        s1.cart.push(
            {id:Product.id,size:Product.size,crust:Product.crust,type:Product.type,image:Product.image,
                name:Product.name,desc:Product.desc,quantity:1})
        }
        this.setState(s1);
    }
    removeToCart=(id)=>{
        let s1={...this.state};
        let found=s1.cart.findIndex(cr=>cr.id===id); 
        if(s1.cart[found].quantity===1)
        {
            let search=s1.Products.find(pr=>pr.id===id);
            search.size="";
            search.crust="";
            s1.cart.splice(found,1);
        }
        else
        {
            s1.cart[found].quantity=s1.cart[found].quantity-1;
        }
        this.setState(s1);
    }
    render(){
        let {sizes,crusts,Products,cart,view}=this.state;
        
        let vegPizza=Products.filter(pr=>pr.type=="Pizza"&&pr.veg=="Yes");
        let nonVegPizza=Products.filter(pr=>pr.type=="Pizza"&&pr.veg=="No");
        let sideDishes=Products.filter(pr=>pr.type=="Side Dish");
        let others=Products.filter(pr=>pr.type!="Pizza"&&pr.type!="Side Dish");
        
        return <div className="container-fluid">
            <nav className="navbar navbar-expand-sm navbar-light"> 
            <div className="container">
              <a className="navbar-brand " >MyFavPizza</a>              
              <div className="collapse navbar-collapse">
                <ul className="navbar-nav">
                  <li className="nav-item">
                    <a className="nav-link " onClick={()=>this.handleView(1)}>Veg Pizza</a>
                  </li> 
                  <li className="nav-item">
                    <a className="nav-link " onClick={()=>this.handleView(2)}>Non-Veg Pizza</a>
                  </li> 
                  <li className="nav-item">
                    <a className="nav-link " onClick={()=>this.handleView(3)}>Side Dishes</a>
                  </li> 
                  <li className="nav-item">
                    <a className="nav-link " onClick={()=>this.handleView(4)}>Other Items</a>
                  </li>                                   
                </ul>
              </div>         
              </div>              
          </nav>
            { view!=0?<React.Fragment>
            <div className="row border-top">
                <div className="col-8">
                {   view===1? <div className="row text-center border">
                        {
                    vegPizza.map(veg=> <M1_AN_A8_BLeftPanelVeg cart={cart}
                        Product={veg} sizes={sizes} crusts={crusts} exceed={this.addToCart}
                        removeFromCart={this.removeToCart}   />)
                        }
                    </div>
                    :view===2?<div className="row text-center border">
                        {
                    nonVegPizza.map(non=> <M1_AN_A8_BLeftPanelNonVeg cart={cart}
                        Product={non} sizes={sizes} crusts={crusts} exceed={this.addToCart} 
                        removeFromCart={this.removeToCart}      />)
                        }
                    </div>
                    :view===3?<div className="row text-center border">
                    {
                    sideDishes.map(sd=><M1_AN_A8_BLeftPanelSide cart={cart}
                    Product={sd} exceed={this.addToCart} 
                    removeFromCart={this.removeToCart}      />)
                    }
                    </div>
                    :<div className="row text-center border">
                    {
                    others.map(odr=><M1_AN_A8_BLeftPanelOther cart={cart}
                    Product={odr} exceed={this.addToCart} 
                    removeFromCart={this.removeToCart}      />)
                    }
                    </div>        
                }
                </div>
                <div className="col-4">
                   {cart.length===0?<h3>Cart is Empty</h3>
                   :<M1_AN_A8_BCart exceed={this.addToCart} cart={cart} removeFromCart={this.removeToCart} />
                   }
                </div>
            </div></React.Fragment>:""
            }
        </div>
    }
}
export default M1_AN_A8_BMainCompo;