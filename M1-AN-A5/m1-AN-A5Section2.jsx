import React, {Component} from "react";
class M1_AN_A5Section2 extends Component{
    state={
        players:[
            {name:"James", points:0},
            {name:"Julia", points:0},
            {name:"Martha", points:0},
            {name:"Steve", points:0},                     
        ],

       questions: [   
           {
               text: "What is the capital of India",     
               options: ["New Delhi", "London", "Paris", "Tokyo"],     
               answer: 1   
            },   
            {     
                text: "What is the capital of France",     
                options: ["New Delhi", "New York", "Paris", "Rome"],     
                answer: 3   
            },   
            {     
                text: "What is the currency of UK",     
                options: ["Dollar", "Mark", "Yen", "Pound"],     
                answer: 4   
            },   
            {     
                text: "What is the height of Mount Everest",     
                options: ["9231 m", "8848 m", "8027 m", "8912 m"],     
                answer: 2   
            },   
            {     
                text: "What is the capital of Japan",     
                options: ["Beijing", "Osaka", "Kyoto", "Tokyo"],     
                answer: 4   
            },   
            {     
                text: "What is the capital of Egypt",     
                options: ["Cairo", "Teheran", "Baghdad", "Dubai"],     
                answer: 1   
            } 
        ], 
        playerIndex:-1,
        idx:0,
    };
    bgcolor=(index)=>{
        let {playerIndex}=this.state;
        let color=index===playerIndex?"bg-success":"bg-warning";
        return color;
    }
    handlePlayerIndex=(index)=>{
        let s1={...this.state};
        s1.playerIndex=index;
        this.setState(s1);
    }
    handleAns=(index)=>{
        let s1={...this.state};
        if(s1.playerIndex===-1){alert("Select player to Answer");}
        else{
        s1.questions[s1.idx].answer===index+1?alert("Correct Answer.You get 3 points"):alert("Wrong Answer.You lose 1 point");
        let marks=s1.questions[s1.idx].answer===index+1?3:-1;
        s1.players[s1.playerIndex].points=s1.players[s1.playerIndex].points+marks;
        s1.idx++;
        s1.playerIndex=-1;
        this.setState(s1);
        }
    }
    maxPoint=()=>{
        let {players}=this.state;
        let winr=players.reduce((acc,curr)=>{
            acc=acc.points<curr.points?curr:acc;
            return acc;
        });
        return winr;
    }
    render(){
        let {players,questions,playerIndex,idx}=this.state; 
         let winner=this.maxPoint();      
         let multiple=players.filter(pr=>pr.points===winner.points);
         let winnerList=multiple.map((ml)=>{ return ml.name});
         winnerList=winnerList.join(',');
         
        return <div className="container text-center">
            <h2>Welcome to the Quiz Contest</h2>
            <h5>Participants</h5>
            <div className="row">
            {
                players.map((p1,index)=>{
                    let {name,points}=p1;
                     return <div className={"col-3 border border-3 border-white "+this.bgcolor(index)}>
                        <b>Name : {name} </b><br/>
                        Score : {points}  <br/>
                        <button className="btn btn-light border m-2" onClick={()=>{this.handlePlayerIndex(index)}}>BUZZER</button>
                    </div>
                })
            }
            </div>
            { idx<4?<React.Fragment>
           <h4>Question Number : {idx+1}</h4>
            <h5>{questions[idx].text}</h5>
            <div className="row m-3 text-center">
                {
                questions[idx].options.map((q1,index)=>{
                    return <div className="col" >
                        <button className="btn btn-info p-2 text-white" onClick={()=>this.handleAns(index)}>{q1}</button>
                    </div>
                })
                }
            </div>
            </React.Fragment>:<React.Fragment>
                <h4>Game Over</h4> {multiple.length===1?<h6> The Winner is {winner.name}
                </h6>:<h6> There is a tie. The Winners are {winnerList}    </h6>
                }
                </React.Fragment>}
        </div>

    }
}
export default M1_AN_A5Section2;