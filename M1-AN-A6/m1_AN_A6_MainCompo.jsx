
import React, {Component} from "react";
import M1_AN_A6_AllMatchs from "./m1_AN_A6_AllMatch";
import M1_AN_A6_PointsTable from "./m1_AN_A6_PointsTable";
import M1_AN_A6_NewMatch from "./m1_AN_A6_NewMatch";
import M1_AN_A6_Match from "./m1_AN_A6_Match";
class M1_AN_A6_MainCompo extends Component{
    state={
        matches:[],
        teams:["France","England","Brazil","Germany","Argentina"],
        team1:"",
        team2:"",
        data:[
          {team:"France",played:0,won:0,lost:0,drawn:0,goalsFor:0,goalsAgainst:0,points:0},
          {team:"England",played:0,won:0,lost:0,drawn:0,goalsFor:0,goalsAgainst:0,points:0},
          {team:"Brazil",played:0,won:0,lost:0,drawn:0,goalsFor:0,goalsAgainst:0,points:0},
          {team:"Germany",played:0,won:0,lost:0,drawn:0,goalsFor:0,goalsAgainst:0,points:0},
          {team:"Argentina",played:0,won:0,lost:0,drawn:0,goalsFor:0,goalsAgainst:0,points:0},
      ],

        view:0,
    };
    updateData=()=>{
      let s1={...this.state};
            
      if(s1.matches.length>0)
      {
      s1.data[0]=this.calculate(0);
      s1.data[1]=this.calculate(1);
      s1.data[2]=this.calculate(2);
      s1.data[3]=this.calculate(3);
      s1.data[4]=this.calculate(4);      
      this.setState(s1);  
      }                           
          
  }
  calculate=(id)=>{      
      let {matches,data}=this.state;        
      let match=matches.filter(ms=>{ return data[id].team===ms.team1||data[id].team===ms.team2});        
      let arr={};
      if(match)
      {
      let team=data[id].team;
      let played=match.length;
      let won=match.filter(ms=>{ return team + " Won"===ms.winner});
      let drawn=match.filter(ms=>{ return "Match Drawn"===ms.winner});
      let lost=played-(won.length+drawn.length);
      let goalsFor=match.reduce((acc,curr)=>{
          curr.team1===team?acc=acc+curr.team1Score:acc=acc+curr.team2Score;
          return acc;
      },0);
      let goalsAgainst=match.reduce((acc,curr)=>{
          curr.team1===team?acc=acc+curr.team2Score:acc=acc+curr.team1Score;
          return acc;
      },0);
      let points=won.length*3+drawn.length*1;
      arr={team:team,played:played,won:won.length,lost:lost,drawn:drawn.length,goalsFor:goalsFor,goalsAgainst:goalsAgainst,points:points};
      
      }
      else
      {
          arr= this.props.data[id];
      }
      return arr;
  }  
  handleSort=(col)=>{
    let s1={...this.state};
    let {data}=s1;
        switch(col)
		{
			case 0:data.sort((p1,p2)=>p1.team.localeCompare(p2.team));break;
      case 1:data.sort((p1,p2)=>p2.played-p1.played);break;
      case 2:data.sort((p1,p2)=>p2.won-p1.won);break;
      case 3:data.sort((p1,p2)=>p2.lost-p1.lost);break;
      case 4:data.sort((p1,p2)=>p2.drawn-p1.drawn);break;
      case 5:data.sort((p1,p2)=>p2.goalsFor-p1.goalsFor);break;
      case 6:data.sort((p1,p2)=>p2.goalsAgainst-p1.goalsAgainst);break;
      case 7:data.sort((p1,p2)=>p2.points-p1.points);break;	
			
		}
       this.setState(s1);
  }            
    handleView=(v,str1="",str2="")=>{
        let s1={...this.state};
        s1.view=v;
        s1.team1=str1;
        s1.team2=str2;
        if(s1.view===2){ this.updateData()}
        this.setState(s1);
       
    }
    handleInsertData=(v,arr)=>{
      let s1={...this.state};
        s1.view=v;
        s1.matches.push(arr);
        s1.team1="";
        s1.team2="";
        this.setState(s1);
    }
    render(){
        let {matches,view,teams,team1,team2,data}=this.state;
                
        return (
            <div className="container-fluid">
           <nav className="navbar navbar-expand-sm navbar-dark bg-dark">
            <div className="container-fluid">
              <a className="navbar-brand" >Football Tournament</a>              
              <div className="collapse navbar-collapse">
                <ul className="navbar-nav">
                  <li className="nav-item">
                    <a className="nav-link ">Number of Matches                    
                    <span className="badges btn btn-primary btn-sm rounded-circle text-white">{matches.length}</span></a>                    
                  </li>                  
                </ul>
              </div>
            </div>
          </nav>
          {view!=4
          ?<React.Fragment><button className="btn btn-primary m-3" onClick={()=>this.handleView(1)}>
            All Matches</button>
          <button className="btn btn-primary m-3" onClick={()=>this.handleView(2)}>
            Points Table</button>
          <button className="btn btn-primary m-3" onClick={()=>this.handleView(3)}>
            New Match</button>
            </React.Fragment>:""}
          { view===0
          ?""         
          :view===1?
          <M1_AN_A6_AllMatchs matches={matches} />
          :view===2
          ?<M1_AN_A6_PointsTable data={data} onSort={this.handleSort} />
          :view===3
          ?<M1_AN_A6_NewMatch teams={teams} team1={team1} team2={team2} 
            changeView={this.handleView}  />
          :<M1_AN_A6_Match team1={team1} team2={team2} onSubmit={this.handleInsertData} />
          }
            </div>
        )
    }
}
export default M1_AN_A6_MainCompo;