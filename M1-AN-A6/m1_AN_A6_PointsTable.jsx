import React, {Component} from "react";
class M1_AN_A6_PointsTable extends Component{
    state={
        data:this.props.data, 
        
    };       
    Sort=(col)=>{
        this.props.onSort(col);
    }
    render(){   
        let {data}=this.state;
                
        return (
            <div className="container text-center">                
                <h4>Points Table</h4>
                <div className="row bg-dark text-white border">
                    <div className="col-3" onClick={()=>this.Sort(0)}>Team</div>
                    <div className="col-1" onClick={()=>this.Sort(1)}>Played</div>
                    <div className="col-1" onClick={()=>this.Sort(2)}>Won</div>
                    <div className="col-1" onClick={()=>this.Sort(3)}>Lost</div>
                    <div className="col-1" onClick={()=>this.Sort(4)}>Drawn</div>
                    <div className="col-2" onClick={()=>this.Sort(5)}>Goals For</div>
                    <div className="col-2" onClick={()=>this.Sort(6)}>Goals Against</div>
                    <div className="col-1" onClick={()=>this.Sort(7)}>Points</div>
                </div>
                {
                    data.map(dt=><div className="row  border">
                    <div className="col-3">{dt.team}</div>
                    <div className="col-1">{dt.played}</div>
                    <div className="col-1">{dt.won}</div>
                    <div className="col-1">{dt.lost}</div>
                    <div className="col-1">{dt.drawn}</div>
                    <div className="col-2">{dt.goalsFor}</div>
                    <div className="col-2">{dt.goalsAgainst}</div>
                    <div className="col-1">{dt.points}</div>
                    </div>)
                }
            </div>
        )
    }
}
export default M1_AN_A6_PointsTable;