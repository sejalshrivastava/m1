import React,{Component} from "react";
import M1_AN_A7_DisplayData from "./m1_AN_A7_DisplayData";
import M1_AN_A7_AddProductForm from "./m1_AN_A7_AddProductForm";
import M1_AN_A7_StockRecieve from "./m1_AN_A7_StockRecieve";
class M1_AN_A7_MainCompo extends Component{
    state={
        Brands:["Nestle", "Haldiram", "Pepsi", "Coca Cola", "Britannia", "Cadburys", "P&G", 
                "Colgate", "Parachute","Gillete", "Dove", "Levis", "Van Heusen", 
                "Manyavaar", "Zara"],
        Products:[
            {
                code: "PEP1253", price: 20, brand: "Pepsi", category: "Food",
                specialOffer: false, limitedStock: false, quantity: 25
            },
            {
                code: "MAGG021", price: 25, brand: "Nestle", category: "Food",
                specialOffer: true, limitedStock: true, quantity: 10
            },
            {
                code: "LEV501", price: 1000, brand: "Levis", category: "Apparel",
                specialOffer: true, limitedStock: true, quantity: 3
            },
            {
                code: "CLG281", price: 60, brand: "Colgate", category: "Personal Care",
                specialOffer: true, limitedStock: true, quantity: 5
            },
            {
                code: "MAGG451", price: 25, brand: "Nestle", category: "Food",
                specialOffer: true, limitedStock: true, quantity: 0
            },
            {
                code: "PAR250", price: 40, brand: "Parachute", category: "Personal Care",
                specialOffer: true, limitedStock: true, quantity: 5
            }
        ],
        view:0,
        editIndex:-1,
        newStock:[],
    };
    handleView=(vw)=>{
        let s1={...this.state};
        s1.view=vw;
        s1.editIndex=-1;
        this.setState(s1);
    }
    handleEditIndex=(idx)=>{
        let s1={...this.state};
        s1.view=1;
        s1.editIndex=idx;
        this.setState(s1);
    }
    handleSubmit=(product)=>{
        let s1={...this.state};
        s1.editIndex>=0?s1.Products[s1.editIndex]=product:s1.Products.push(product);
        s1.view=0;
        s1.editIndex=-1;
        this.setState(s1);
    }
    handleStock=(stock)=>{
        let s1={...this.state};
        s1.view=0;
        let idx=s1.Products.findIndex(pr=>pr.code===stock.code);
        s1.Products[idx].quantity=s1.Products[idx].quantity+(+stock.quantity);        
        s1.newStock.push(stock);
        this.setState(s1);
    }
    render(){
        let {Brands,Products,view,editIndex}=this.state;
        let codes=Products.reduce((acc,curr)=>{ acc.push(curr.code);return acc},[]);
        let stock={code:"",year:"",month:"",day:"",quantity:""};
         let qty=Products.reduce((acc,curr)=>{acc=acc+curr.quantity;return acc},0);
         let val=Products.reduce((acc,curr)=>{acc=acc+curr.quantity*curr.price;return acc},0);
        let product=editIndex>=0?Products[editIndex]
            :{code:"",price:"",category:"",brand:"",specialOffer:false,limitedStock:false}; 
        return <div className="container">
            <nav className="navbar navbar-expand-sm navbar-dark bg-dark">            
              <a className="navbar-brand" >ProdStoreSys</a>              
              <div className="collapse navbar-collapse">
                <ul className="navbar-nav">
                  <li className="nav-item">
                  <a className="nav-link " >Products
                    <span className="bagde bg-secondary m-1 p-1 rounded-circle text-white">{Products.length}</span></a>
                  </li>
                  <li className="nav-item">
                  <a className="nav-link " >Quantity
                    <span className="bagde bg-secondary m-1 p-1 rounded-circle text-white">{qty}</span></a>
                  </li>
                  <li className="nav-item">
                  <a className="nav-link " >Value
                    <span className="bagde bg-secondary m-1 p-1 rounded-circle text-white">{val}</span></a>
                  </li>                  
                </ul>
              </div>
            
          </nav>
            {
                view===0
                ?<M1_AN_A7_DisplayData Products={Products} 
                    changeView={this.handleView} 
                    handleEdit={this.handleEditIndex} />
                :view===1
                ?<M1_AN_A7_AddProductForm 
                    Brands={Brands} product={product} codes={codes} 
                    changeView={this.handleView} 
                    editIndex={editIndex}
                    onSubmit={this.handleSubmit} />
                :<M1_AN_A7_StockRecieve 
                    stock={stock} 
                    onSubmit={this.handleStock}
                    changeView={this.handleView} 
                    codes={codes} />
            }
        </div>
    }
}
export default M1_AN_A7_MainCompo;