import React, {Component} from "react";
class M1_AN_A6_NewMatch extends Component{
    state ={
        teams:this.props.teams,
        team1:this.props.team1,
        team2:this.props.team2,
    };
    handleTeam1=(opt)=>{
        let s1={...this.state};
        s1.team2===opt?alert("Select Different Teams"):s1.team1=opt;
        this.setState(s1);
    }
    handleTeam2=(opt)=>{
        let s1={...this.state};
        s1.team1===opt?alert("Select Different Teams"):s1.team2=opt;
        this.setState(s1);
    }
    handleView=()=>{
        let s1={...this.state};
        s1.team1===""
        ?alert("Select Team 1")
        :s1.team2===""
        ?alert("Select Team 2")
        :this.props.changeView("4",s1.team1,s1.team2);        
    }
    render(){
        let  {teams,team1,team2}=this.state;
        
        return (
            <div className="container text-center">
                {team1?<h4>Team 1 : {team1} </h4>:<h4>Choose Team 1</h4>}
                {teams.map(opt=><button className="btn btn-warning m-3" 
                    onClick={()=>this.handleTeam1(opt)}>{opt}</button>)}

                {team2?<h4>Team 2 : {team2} </h4>:<h4>Choose Team 2</h4>}
                {teams.map(opt=><button className="btn btn-warning m-3" 
                    onClick={()=>this.handleTeam2(opt)}>{opt}</button>)}

                <br/>
                <button className="btn btn-secondary  m-4"
                    onClick={()=>this.handleView()}>Start Match</button>
            </div>
        )
    }
}
export default M1_AN_A6_NewMatch;