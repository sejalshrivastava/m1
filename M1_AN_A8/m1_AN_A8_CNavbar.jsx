import React, {Component} from "react";
import {Link} from "react-router-dom";
class M1_AN_A8_CNavbar extends Component{
    
    render(){
        return <nav className="navbar navbar-expand-sm navbar-dark bg-dark">            
              <Link className="navbar-brand" to="/">MyCompany</Link>              
              <div className="collapse navbar-collapse">
                <ul className="navbar-nav">
                  <li className="nav-item">
                    <Link className="nav-link " to="/emps">All</Link>
                  </li>
                  <li className="nav-item">
                    <Link className="nav-link" to="/emps/New Delhi">New Delhi</Link>
                  </li>                  
                  <li className="nav-item">
                    <Link className="nav-link" to="/emps/Noida">Noida</Link>
                  </li>                 
                </ul>
              </div>
            
          </nav>
        
    }
}
export default M1_AN_A8_CNavbar;