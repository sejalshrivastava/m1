import React, {Component} from "react";
class M1_AN_A6_AllMatchs extends Component{
    
    render(){
        let {matches}=this.props;
        return (
            <div className="container text-center ">
                {matches.length===0
                ?<h4>There are no matches</h4>
                :<React.Fragment>
                    <h5>Results of the matches so far</h5>
                    <div className="row bg-dark text-white border">
                    <div className="col-2">Team1</div>
                    <div className="col-2">Team2</div>
                    <div className="col-2">Score</div>
                    <div className="col-5">Result</div>
                    </div>
                    {
                        matches.map(mt=><div className="row border">
                        <div className="col-2">{mt.team1}</div>
                        <div className="col-2">{mt.team2}</div>
                        <div className="col-2">{mt.team1Score}-{mt.team2Score}</div>
                        <div className="col-5">{mt.winner}</div>
                        </div>)
                    }
                    </React.Fragment>}
            </div>
        )
    }
}
export default M1_AN_A6_AllMatchs;