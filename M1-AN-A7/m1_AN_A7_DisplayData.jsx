import React ,{Component } from "react";
class M1_AN_A7_DisplayData extends Component{
    handleView=(view)=>{
        this.props.changeView(view);
    }
    EditDetails=(index)=>{
        this.props.handleEdit(index);
    }
    render(){
        const {Products}=this.props;
        
        return <div className="container">
            <div className="row text-center">
                {
                Products.map((pr,index)=><div className="col-3 bg-light border p-1" key={pr.code}>
                    <h5>Code : {pr.code}</h5>
                    <h6>Brand : {pr.brand}</h6>
                    <h6>Category : {pr.category}</h6>
                    <h6>Price : {pr.price} </h6>
                    <h6>Quantity : {pr.quantity}</h6>
                    <h6>Special Offer : {pr.specialOffer?"Yes":"No"}</h6>
                    <h6>Limited Stock : {pr.limitedStock?"Yes":"No"} </h6>
                    <button className="btn btn-warning btn-sm m-1" onClick={()=>this.EditDetails(index)}>Edit Details</button>
                </div>)
                }
            </div>
                <button className="btn btn-primary m-3" onClick={()=>this.handleView(1)}>Add New Product</button>
                <button className="btn btn-primary m-3" onClick={()=>this.handleView(2)}>Recieve Stocks</button>
        </div>
    }
}
export default M1_AN_A7_DisplayData;