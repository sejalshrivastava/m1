import React, {Component} from "react";
class M1_AN_A5Section1 extends Component{
    state={
    products :[
        {product:"Pepsi", sales:[2,5,8,10,5]},
        {product:"Coke", sales:[3,6,5,4,11,5]},
        {product:"5Star", sales:[10,14,22]},
        {product:"Maggi", sales:[3,3,3,3,3]},
        {product:"Perk", sales:[1,2,1,2,1,2]},
        {product:"Bingo", sales:[0,1,0,3,2,6]},
        {product:"Gems", sales:[3,3,1,1]},
        ],
        listView:-1,
    };
    totalSales=(sales)=>sales.reduce((acc,curr)=>acc+=curr);
    sortProd=()=>{
        let s1={...this.state};
        s1.listView=-1;
        s1.products=s1.products.sort((p1,p2)=>{return p1.product.localeCompare(p2.product)});        
        this.setState(s1);
    }
    salesAsc=()=>{
        let s1={...this.state};
        s1.listView=-1;
        s1.products=s1.products.sort((p1,p2)=>{
            let sales1=p1.sales;
            let sale1=this.totalSales(sales1);
            let sales2=p2.sales;            
            let sale2=this.totalSales(sales2);
            return sale1-sale2});        
        this.setState(s1);
    }
    salesDesc=()=>{
        let s1={...this.state};
        s1.listView=-1;
        s1.products=s1.products.sort((p1,p2)=>{
            let sales1=p1.sales;
            let sale1=this.totalSales(sales1);
            let sales2=p2.sales;            
            let sale2=this.totalSales(sales2);
            return sale2-sale1});        
        this.setState(s1);
    }
    showDetails=(index)=>{
        let s1={...this.state};
        s1.listView=index;
        this.setState(s1);
    }
    render(){
        let {products,listView}=this.state;
        return <div className="container">
            <h3 className="text-center">Products </h3>
            <button className="btn btn-primary m-3" onClick={()=>{this.sortProd()}}>Sort By Product</button>
            <button className="btn btn-primary m-3" onClick={()=>{this.salesAsc()}}>Total Sales Asc.</button>
            <button className="btn btn-primary m-3" onClick={()=>{this.salesDesc()}}>Total Sales Desc.</button>
            <div className="row bg-dark text-white text-center ">
                <div className="col-4 border p-2">Product</div>
                <div className="col-4 border p-2">Total Sales</div>
                <div className="col-4 border p-2">Details</div>
            </div>
            {
                products.map((p1,index)=>{
                    let {product,sales}=p1;
                    return <div className="row text-center">
                    <div className="col-4 border p-2">{product}</div>
                    <div className="col-4 border p-2">{this.totalSales(sales)}</div>
                    <div className="col-4 border p-2">
                        <button className="btn btn-primary btn-sm m-1" onClick={()=>this.showDetails(index)}>Details</button>
                    </div>
                </div>
                })
            }
            {listView>=0?<ul className="m-2">
                <h4>{products[listView].product}</h4>
                <h6>Sales of {products[listView].product} in Various Stores</h6>
                {
                    products[listView].sales.map(pr=><li>{pr} </li>)
                }
            </ul>:""}
        </div>
    }
}
export default M1_AN_A5Section1;