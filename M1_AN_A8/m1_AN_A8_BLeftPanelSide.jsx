import React,{Component} from "react";
class M1_AN_A8_BLeftPanelSide extends Component{
    state={
        Product:this.props.Product,
    };
    handleChange=(e)=>{
        let {currentTarget:input}=e;
        let s1={...this.state};
        s1.Product[input.name]=input.value;
        this.setState(s1);
    }    
    addToCart=()=>{
        let{Product}=this.state;        
        this.props.exceed(Product);
    }    
    removeToCart=(id)=>{
        this.props.removeFromCart(id)
    }
    render(){
        let {cart}=this.props;
        let {Product}=this.state;   
        
        let {id,image,name,desc}=Product;                         
        let inCart=cart.find(cr=>cr.id===id);
        return <React.Fragment>
            {inCart?<div className="col-6 text-center border">                        
                        <img src={image} width="94%"></img>
                        <h4>{name}</h4>
                        {desc}
                        <br/>
                        <button className="btn btn-danger m-1 text-white"
                            onClick={()=>this.removeToCart(id)}>-</button>
                        <button className="btn btn-secondary m-1 text-white">{inCart.quantity}</button>
                        <button className="btn btn-success m-1 text-white" 
                            onClick={()=>this.addToCart()}>+</button>
                    
                    </div>
                    :<div className="col-6 text-center border">                        
                        <img src={image} width="94%"></img>
                        <h4>{name}</h4>
                        {desc}
                        <br/>
                        <button className="btn btn-primary m-1" 
                            onClick={()=>this.addToCart()}>Add to Cart</button>
                    
                    </div>
                    }
                </React.Fragment>         
            
    }
}
export default M1_AN_A8_BLeftPanelSide;