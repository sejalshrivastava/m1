import React,{Component} from "react";
class M1_AN_A8_ABill extends Component{
    state={
        bills:this.props.bills,
    };
    addProduct=(code)=>{
        this.props.exceedProd(code);
    }
    reduceToProd=(code)=>{
        this.props.reduceProd(code);
    }
   RmvToProd=(code)=>{
        this.props.handleRem(code);
    }
    handleBillsClr=()=>{
        this.props.clearBills();
    }
    render(){
        let {bills}=this.state;
        let items=bills.length;
        let qty=bills.length>0?bills.reduce((acc,curr)=>acc=acc+curr.quantity,0):0;
        let amt=bills.length>0?bills.reduce((acc,curr)=>acc=acc+curr.quantity*curr.price,0):0;
        return <React.Fragment>
            <h4 className="mt-2">Details of Current Bill</h4>
            <h6>Items :{items} ,Quantity :{qty} ,Amount :{amt}</h6>
            {
                bills.length>0?<React.Fragment>
                {
                bills.map((bill,index)=>
                    <div className="row border " key={bill.code}>
                        <div className="col-6">{bill.code},{bill.prod},
                            Price :{bill.price},Quantity :{bill.quantity},
                            Value :{bill.price*bill.quantity}
                            </div>
                        <div className="col-6 ">
                            <button className="btn btn-success text-white btn-sm m-1" 
                                onClick={()=>this.addProduct(bill.code)}>+</button>
                            <button className="btn btn-warning btn-sm m-1"
                                onClick={()=>this.reduceToProd(bill.code)}>-</button>
                            <button className="btn btn-danger text-white btn-sm m-1"
                                onClick={()=>this.RmvToProd(bill.code)}>x</button>
                        </div>
                    </div>
                )}
                <button className="btn btn-primary btn-sm mt-1" 
                    onClick={()=>this.handleBillsClr()}>Close Bill</button>
                </React.Fragment>:""
            }
        </React.Fragment>
    }
}
export default M1_AN_A8_ABill;