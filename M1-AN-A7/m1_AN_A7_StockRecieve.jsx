import React,{Component} from "react";
class M1_AN_A7_StockRecieve extends Component{
    state={
        stock:this.props.stock,
    };
    handleSubmit=()=>{
        let {stock}=this.state;        
        if(stock.code==="")
        {
            alert("Select Product Code");
        }
        else if(stock.quantity==="")
        {
            alert("Enter the quantity");
        }
        else if(stock.year===""||stock.month===""||stock.day=="")
        {
            alert("Select the Date");
        }
        else
        {
        this.props.onSubmit(stock);
        }
    }
    handleChange=(e)=>{
        let s1={...this.state};
        let {currentTarget:input}=e;
        s1.stock[input.name]=input.value;
        if(input.name==="month"||input.name==="year")s1.stock.day="";
        this.setState(s1);
    }
    handleView=(view)=>{
        this.props.changeView(view);
    }
    makeDropDown=(arr,label,name,value)=>(
        <div className="form-group m-2">
            <select
                className="form-control"
                name={name}
                value={value}
                onChange={this.handleChange} >
                    <option value="">{label}</option>
                {
                   arr.map(opt=><option>{opt}</option>)
                }    
           </select>
            </div>
    )
    render(){
        let {codes}=this.props;
        let {code,year,month,day,quantity}=this.state.stock;
        
        let years=[];
        for(let i=1900;i<=2025;i++)
        {
            years.push(i);
        } 
        let months=[{display:"January",value:1},{display:"February",value:2},{display:"March",value:3},
                {display:"April",value:4},{display:"May",value:5},{display:"June",value:6},
                {display:"July",value:7},{display:"August",value:8},{display:"September",value:9},
                {display:"October",value:10},{display:"November",value:11},{display:"December",value:12},];
        
        let lst=""  ;
        
        if(year&&month)
        {           
                if((month==2&&year%4==0)||(month==2&&year%4==100)||(month==2&&year%4==400))
                {
                    
                    lst=29;
                }
                else if(month==1||month==8||month==3||month==5||month==7||month==12||month==10)
                {
                    lst=31;
                }
                else if(month===2)
                {
                    lst=28;
                }                
                else 
                {
                    lst=30;
                }
               
        }       
        let days=[];
        if(lst){
            for(let i=1;i<=lst;i++)
            {
                days.push(i);
            } 
        }              
        
        return <div className="container">
            <h4>Select the Product whose stocks have been recieved</h4>
            {this.makeDropDown(codes,"Select Product Code","code",code)}  
            <div className="form-group">
                <label className="form-label">Stocks Recieved</label>
                <input className="form-control"
                    type="text"
                    placeholder="Enter Number of Stocks"
                    value={quantity}
                    name="quantity"                    
                    onChange={this.handleChange}
                      />                
            </div>          
            <div className="row">
                <div className="col-4">
                {this.makeDropDown(years,"Select Year","year",year)}
                </div>
                <div className="col-4">
                    <div className="form-group m-2">
                    <select
                    className="form-control"
                    name="month"
                    value={month}
                    onChange={this.handleChange} >
                    <option value="">Select Month</option>
                    {
                    months.map(opt=><option value={opt.value}>{opt.display}</option>)
                    }    
                    </select>
                    </div>
                </div>
                <div className="col-4">
                {this.makeDropDown(days,"Select Day","day",day)}
                </div>
            </div>

            <button className="btn btn-primary mt-1 btn-sm"
                onClick={()=>this.handleSubmit()} >Submit</button>
            <br/>
            <button className="btn btn-primary mt-4" onClick={()=>this.handleView(0)} >Go Back To Homepage</button>
        </div>
    }
}
export default M1_AN_A7_StockRecieve; 